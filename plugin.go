package scream

import (
	"plugin"

	"github.com/pkg/errors"
)

func (a *App) loadPlugin(path string) error {
	p, err := plugin.Open(path)
	if err != nil {
		return errors.Wrapf(err, "could not load plugin from %q", path)
	}

	// TODO: ???
	_ = p

	return nil
}
