package scream

// Machine-readable version information (filled in by build server).
const (
	MajorVersion = 0
	MinorVersion = 0
	PatchVersion = 0
	BuildNumber  = 0
	CommitHash   = ""
)
