package scream

import "net/http"

type errorHandler struct {
	Status int
	Error  string
}

func (e *errorHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	http.Error(w, e.Error, e.Status)
}

func (a *App) NotFoundHandler() http.Handler {
	return &errorHandler{
		Status: http.StatusNotFound,
		Error:  "Not Found",
	}
}

func (a *App) MethodNotAllowedHandler() http.Handler {
	return &errorHandler{
		Status: http.StatusMethodNotAllowed,
		Error:  "Method Not Allowed",
	}
}

func (a *App) InternalServerErrorHandler() http.Handler {
	return &errorHandler{
		Status: http.StatusInternalServerError,
		Error:  "Internal Server Error",
	}
}

func (a *App) BadRequestHandler() http.Handler {
	return &errorHandler{
		Status: http.StatusBadRequest,
		Error:  "Bad Request",
	}
}
