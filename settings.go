package scream

import "context"

type Config struct {
	Plugins         []string `ini:"plugin,omitempty,allowshadow" comment:"Enabled plugin paths"`
	AllowFederation bool     `ini:"allow_federation" comment:"Whether to enable the ActivityPub server-to-server API"`
}

func (a *App) NewConfiguration() interface{} {
	return &Config{
		AllowFederation: true,
	}
}

func (a *App) SetConfiguration(v interface{}) error {
	a.config = v.(*Config)

	for _, path := range a.config.Plugins {
		if err := a.loadPlugin(path); err != nil {
			return err
		}
	}

	return nil
}

func (a *App) C2SEnabled() bool {
	return true
}

func (a *App) S2SEnabled() bool {
	return a.config.AllowFederation
}

func (a *App) MaxInboxForwardingRecursionDepth(ctx context.Context) int {
	return 0
}

func (a *App) MaxDeliveryRecursionDepth(ctx context.Context) int {
	return 0
}
