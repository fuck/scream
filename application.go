package scream

import (
	"context"
	"net/url"

	"github.com/go-fed/activity/streams/vocab"
	"github.com/go-fed/apcore"
	"github.com/pkg/errors"
)

// App is the implementation of Scream.
type App struct {
	config *Config
}

// NewId is a deprecated alias of NewID.
func (a *App) NewId(ctx context.Context, obj vocab.Type) (*url.URL, error) { //nolint:golint
	return a.NewID(ctx, obj)
}

// NewID generates a unique IRI for a new ActivityStreams object.
func (a *App) NewID(ctx context.Context, obj vocab.Type) (*url.URL, error) {
	return nil, errors.New("NewID is not yet implemented")
}

func (a *App) ScopePermitsPostOutbox(scope string) (bool, error) {
	return false, errors.New("ScopePermitsPostOutbox is not yet implemented")
}

// Software returns information about this application's software.
func (a *App) Software() apcore.Software {
	return apcore.Software{
		Name:         "scream",
		MajorVersion: MajorVersion,
		MinorVersion: MinorVersion,
		PatchVersion: PatchVersion,
	}
}
